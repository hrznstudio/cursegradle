# Horizon CurseGradle

Originally created by matthewprenger https://github.com/matthewprenger/CurseGradle altered for our uses

A gradle plugin for publishing artifacts to [CurseForge](http://minecraft.curseforge.com/).

## Simple Quickstart with ForgeGradle
If you're using ForgeGradle, which you probably are, the following script is a bare-minimum.

To find out which versions are available, check [HERE](https://plugins.gradle.org/plugin/com.hrznstudio.cursegradle).

```gradle
plugins {
    id 'net.minecraftforge.gradle.forge' version '2.0.2'
    id 'com.hrznstudio.cursegradle' version '<VERSION>'
}

curseforge {
  apiKey = '123-456' // This should really be in a gradle.properties file
  project {
    id = '12345'
    changelog = 'Changes' // A file can also be set using: changelog = file('changelog.txt')
    releaseType = 'beta'
  }
}
```
